package mqnet

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/fkmatsuda.dev/go/fk_mq/mq/config"
)

// Listen starts the HTTP server
func Listen() {
	routes := InitRoutes()
	if config.SSL {
		log.Printf("Starting HTTPS server on %d\nCertificate File: %s\nServer Key File: %s\n", config.Port, config.CertFile, config.KeyFile)
		log.Fatal(http.ListenAndServeTLS(fmt.Sprintf(":%d", config.Port), config.CertFile, config.KeyFile, routes))
	} else {
		log.Printf("Starting HTTP server on %d\n", config.Port)
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), routes))
	}
}
