package mqnet

import (
	"github.com/gorilla/mux"
	"gitlab.com/fkmatsuda.dev/go/fk_mq/mq/mqnet/handler"
)

// InitRoutes - initializes the routes for the application
func InitRoutes() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", handler.HandleOptions).Methods("OPTIONS")
	router.HandleFunc("/", handler.HandleGet).Methods("GET")
	router.HandleFunc("/", handler.HandlePost).Methods("POST")
	router.HandleFunc("/", handler.HandleDelete).Methods("DELETE")
	router.HandleFunc("/sock", handler.HandleWS).Methods("GET")

	return router
}
