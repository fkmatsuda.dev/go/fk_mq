package handler

import (
	"log"
	"net/http"

	"gitlab.com/fkmatsuda.dev/go/fk_mq/mq/config"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
)

var wsPool map[uuid.UUID]*websocket.Conn

func init() {
	wsPool = make(map[uuid.UUID]*websocket.Conn)
}

// HandleWS - handles ws connections
func HandleWS(w http.ResponseWriter, r *http.Request) {

	headerFCKCORS(w)

	upgrader := websocket.Upgrader{}
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		handleError(w, err)
		return
	}

	registerWS(ws)

}

func registerWS(ws *websocket.Conn) {
	socketID := uuid.New()
	wsPool[socketID] = ws
	go func() {
		defer unregisterWS(socketID)
		for {
			messageType, reader, err := ws.NextReader()
			if err != nil {
				log.Default().Printf("reader message error: %v\n", err)
				break
			}
			switch messageType {
			case websocket.TextMessage:
				buffer := make([]byte, config.MaxMessageSize)
				reader.Read(buffer)
				BroadcastMessage(socketID, websocket.TextMessage, buffer)
			}
		}
	}()
}

// BroadcastMessage - broadcasts message to all websocket connections
func BroadcastMessage(socketID uuid.UUID, messageType int, message []byte) {
	for ID := range wsPool {
		if ID != socketID {
			ws := wsPool[ID]
			w, err := ws.NextWriter(messageType)
			if err != nil {
				log.Default().Printf("writer message error: %v\n", err)
			} else {
				defer w.Close()
				w.Write(message)
			}
		}
	}
}

func unregisterWS(socketID uuid.UUID) {
	ws, ok := wsPool[socketID]
	if ok {
		ws.Close()
		delete(wsPool, socketID)
	}
}
