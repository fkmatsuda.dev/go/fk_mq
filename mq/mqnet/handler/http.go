package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"gitlab.com/fkmatsuda.dev/go/fk_mq-client/mqclient/mqmessage"
)

// HandleGet handles GET requests
func HandleGet(w http.ResponseWriter, r *http.Request) {
}

// HandlePost handles POST requests
func HandlePost(w http.ResponseWriter, r *http.Request) {
	body := r.Body
	defer body.Close()
	bodyContent, err := ioutil.ReadAll(body)
	if err != nil {
		handleError(w, err)
		return
	}

	var message mqmessage.Message
	if err = json.Unmarshal(bodyContent, &message); err != nil {
		handleError(w, err)
		return
	}

	PostMessage(message)

	headerFCKCORS(w)
	w.WriteHeader(http.StatusCreated)
	w.Write([]byte("sent"))

}

// HandleDelete handles DELETE requests
func HandleDelete(w http.ResponseWriter, r *http.Request) {
}

// HandleOptions handles OPTIONS requests
func HandleOptions(w http.ResponseWriter, r *http.Request) {
	//Allow CORS here By * or specific origin
	headerFCKCORS(w)
	json.NewEncoder(w).Encode("OK")
}

func headerFCKCORS(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

func PostMessage(message mqmessage.Message) error {
	buffer, err := json.Marshal(message)
	if err != nil {
		return err
	}
	BroadcastMessage(uuid.Nil, websocket.TextMessage, buffer)
	return nil
}
