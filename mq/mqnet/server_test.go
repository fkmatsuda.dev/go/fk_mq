package mqnet

import (
	"log"
	"net/url"
	"testing"

	"github.com/google/uuid"
	"gitlab.com/fkmatsuda.dev/go/fk_mq-client/mqclient"
	"gitlab.com/fkmatsuda.dev/go/fk_mq-client/mqclient/mqmessage"
	"gitlab.com/fkmatsuda.dev/go/fk_mq/mq/config"
	"gitlab.com/fkmatsuda.dev/go/fk_mq/mq/mqnet/handler"
)

func TestServer(t *testing.T) {
	config.MaxMessageSize = 1024
	config.Port = 19898
	go Listen()
	message := mqmessage.Message{
		To:      uuid.Nil,
		Tag:     "test",
		Payload: "test",
	}
	u := &url.URL{
		Scheme: "ws",
		Host:   "localhost:19898",
		Path:   "/sock",
	}
	log.Println(u.String())
	c, err := mqclient.Connect(u)
	if err != nil {
		t.Errorf("error when try to connect: %v\nURL: %v", err, c)
		return
	}
	ch := make(chan bool, 1)
	chText := make(chan string, 1)
	c.OnMessage(func(m *mqmessage.Message, err error) {
		if err != nil {
			t.Errorf("error when try to retrieve message: %v", err)
			ch <- false
			return
		}
		chText <- m.Payload
		if m.Payload != message.Payload {
			t.Errorf("message is not equal")
			ch <- false
		}
		log.Default().Println("message is equal")
		ch <- true
	})

	handler.PostMessage(message)

	res := <-ch
	if !res {
		t.Error("result error")
		return
	}
	text := <-chText
	log.Default().Println(text)
}
