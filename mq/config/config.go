package config

var (
	// Port - server port
	Port uint16

	// MaxMessageSize - max message size
	MaxMessageSize uint32

	// SSL - use ssl
	SSL bool

	// CertFile - ssl cert file
	CertFile string

	// KeyFile - ssl key file
	KeyFile string
)
